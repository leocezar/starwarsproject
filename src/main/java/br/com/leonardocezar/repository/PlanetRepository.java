package br.com.leonardocezar.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import br.com.leonardocezar.model.Planet;


@Repository
public interface PlanetRepository extends MongoRepository<Planet, String>{
	
	List<Planet> findByNameLikeOrderByName(String name);

}
