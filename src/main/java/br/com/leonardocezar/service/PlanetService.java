package br.com.leonardocezar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.leonardocezar.model.Planet;
import br.com.leonardocezar.repository.PlanetRepository;
import br.com.leonardocezar.service.exception.ObjectNotFoundException;
import br.com.leonardocezar.template.PlanetTemplate;


@Service
public class PlanetService {
	
	@Autowired
	protected PlanetRepository repository;
	
	@Autowired
	protected SwapiPlanetService swapiPlanetService; 
	
	public List<Planet> findAll(){
		List<Planet> colabodores = repository.findAll();        
        return colabodores;

	}
	
	public Planet findBy(String id) {
		Optional<Planet> obj = repository.findById(id);
		if(!obj.isPresent()) {
			throw new ObjectNotFoundException("O Planet que você está procurando não existe");
		}
		
		return obj.get();
	}
	
	public void update(Planet Planet) {
		repository.save(Planet);
	}
	
	public void delete(String id) {
		repository.delete(this.findBy(id));		
	}
	
	public Planet save(Planet planet) {
		planet.setId(null);
		List<PlanetTemplate> result = swapiPlanetService.findPlanetByName(planet.getName());
		
		if(result != null && result.size() == 1) {
			planet.setMovieAppearances(result.iterator().next().getFilms().size());
		}
		return repository.save(planet);
	}
	
	public List<Planet> buscarPorNome(String nome) {
		List<Planet> lista = repository.findByNameLikeOrderByName(nome);
		return lista;
	}

}
