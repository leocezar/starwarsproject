package br.com.leonardocezar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.leonardocezar.template.BaseTemplate;
import br.com.leonardocezar.template.PlanetTemplate;

@Service
public class SwapiPlanetService extends BaseSwapiService {
	

	public static final String PLANET_PATH = "planets/";
    
	
	@Autowired
	private RestTemplate restTemplate;
	
	public List<PlanetTemplate> findPlanetByName(String name) {
		
		try {

	        HttpEntity<String> entity = setHeaders();

	        String fullPath = swapiUrl + PLANET_PATH + swapiUrlFilter;
			
			BaseTemplate<PlanetTemplate> template = restTemplate.exchange(fullPath+name, HttpMethod.GET, entity, getParameterizedTypeReference()).getBody();
			return template.getResults();	
			
		}catch (Exception e) {
			return null;
		}

	}

	private ParameterizedTypeReference<BaseTemplate<PlanetTemplate>> getParameterizedTypeReference() {
		return new ParameterizedTypeReference<BaseTemplate<PlanetTemplate>>() {};
	}

	
}
