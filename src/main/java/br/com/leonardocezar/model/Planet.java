package br.com.leonardocezar.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "planetas")
public class Planet {
	
	@Id
	private String id;
	private String name;
	private String climate;
	private String ground;
	private Integer movieAppearances;
	/**
	 * Retorna o atributo id.
	 * @return Retorna o atributo id do tipo String.
	 */
	public String getId() {
		return id;
	}
	/**
	 * Especifica o atributo id.
	 * @param id String referente a id a ser setado.
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * Retorna o atributo name.
	 * @return Retorna o atributo name do tipo String.
	 */
	public String getName() {
		return name;
	}
	/**
	 * Especifica o atributo name.
	 * @param name String referente a name a ser setado.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Retorna o atributo climate/clima.
	 * @return Retorna o atributo climate do tipo String.
	 */
	public String getClimate() {
		return climate;
	}
	/**
	 * Especifica o atributo climate/clima.
	 * @param climate String referente a climate/clima a ser setado.
	 */
	public void setClimate(String climate) {
		this.climate = climate;
	}
	/**
	 * Retorna o atributo ground/terreno.
	 * @return Retorna o atributo ground do tipo String.
	 */
	public String getGround() {
		return ground;
	}
	/**
	 * Especifica o atributo ground/terreno.
	 * @param ground String referente a ground a ser setado.
	 */
	public void setGround(String ground) {
		this.ground = ground;
	}
	/**
	 * Retorna o atributo movieAppearances/apariçõesFilmes.
	 * @return Retorna o atributo movieAppearance do tipo Integer.
	 */
	public Integer getMovieAppearances() {
		return movieAppearances;
	}
	/**
	 * Especifica o atributo movieAppearance.
	 * @param movieAppearance Integer referente a movieAppearance a ser setado.
	 */
	public void setMovieAppearances(Integer movieAppearances) {
		this.movieAppearances = movieAppearances;
	}
	
	
	


}
