package br.com.leonardocezar.resources.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.leonardocezar.service.exception.ObjectNotFoundException;

@ControllerAdvice
public class ResourcesExceptionHandler {

	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<DefaultError> handleObjetoNaoEncontrado(ObjectNotFoundException e,HttpServletRequest request){
		DefaultError erro = new DefaultError(e.getMessage(), HttpStatus.NOT_FOUND.value());
		
		return ResponseEntity.status(erro.getStatus()).body(erro);
	}
	
}
