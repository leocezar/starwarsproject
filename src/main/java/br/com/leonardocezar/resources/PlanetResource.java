package br.com.leonardocezar.resources;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.leonardocezar.model.Planet;
import br.com.leonardocezar.service.PlanetService;

@RestController
@RequestMapping("/planetas")
public class PlanetResource {
	
	@Autowired
	PlanetService servicePlanet;
	
	@GetMapping
	public ResponseEntity<List<Planet>> buscarTodos(){
		return ok(servicePlanet.findAll());
	}


	@GetMapping("/{id}")
	public ResponseEntity<Planet> buscarPorId(@PathVariable("id") String id){
		return ok(servicePlanet.findBy(id));
	}

	@PostMapping
	public ResponseEntity<Planet> criar(@RequestBody Planet planet){

	    Planet newPlanet = servicePlanet.save(planet);
		return created(fromCurrentRequest()
						.path("/{id}")
						.buildAndExpand(newPlanet.getId())
						.toUri())
				.body(newPlanet);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> atualizar(@PathVariable("id") String id,@RequestBody Planet Planet){
		Planet.setId(id);
		servicePlanet.update(Planet);
		return ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletar(@PathVariable("id") String id){
		servicePlanet.delete(id);
		return ok().build();
	}
	
	@GetMapping("nomes/{nome}")
	public ResponseEntity<List<Planet>> buscarPorNome(@PathVariable("nome") String nome){
		return ok(servicePlanet.buscarPorNome(nome));
	}
}
