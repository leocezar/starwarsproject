package br.com.leonardocezar.resources;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PlanetResourceTest {

	@Test
	public void testBuscarTodos() {
		get("/planetas").then().body("id", notNullValue());
	}

	@Test
	public void testBuscarPorId() {
		get("/planetas").then().body("id", notNullValue());
	}

	@Test
	public void testCriar() {
		get("/planetas").then().body("id", notNullValue());
	}

	@Test
	public void testAtualizar() {
		get("/planetas").then().body("id", notNullValue());
	}

	@Test
	public void testDeletar() {
		get("/planetas").then().body("id", notNullValue());
	}

	@Test
	public void testBuscarPorNome() {
		get("/planetas").then().body("id", notNullValue());
	}

}
